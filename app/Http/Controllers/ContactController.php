<?php

namespace App\Http\Controllers;

use App\Contact;

class ContactController extends Controller
{

    public function add()
    {
        $token = self::getToken();

        $text = 'error';

        if ($token) {
            $xml = '<Contacts>
                    <row no="1">
                        <FL val="First Name">Scott</FL>
                        <FL val="Last Name">James</FL>
                        <FL val="Email">test@test.com</FL>
                        <FL val="Phone">999999999</FL>
                    </row>
                    <row no="2">
                        <FL val="First Name">Artem</FL>
                        <FL val="Last Name">Sidorov</FL>
                        <FL val="Email">dshnda@gmail.com</FL>
                        <FL val="Phone">0933035000</FL>
                    </row>
                    <row no="3">
                        <FL val="First Name">Fedya</FL>
                        <FL val="Last Name">Panarin</FL>
                        <FL val="Email">fedya@gmail.com</FL>
                        <FL val="Phone">0933035004</FL>
                    </row>
                </Contacts>
                ';
            $url = 'https://crm.zoho.com/crm/private/xml/Contacts/insertRecords?authtoken='.$token.'&scope=crmapi&newFormat=1&xmlData='.urlencode($xml);
            $contacts = file_get_contents($url);

            $text = $contacts;
        }

        return view('contact', ['text' => $text]);
    }

    public function fetch()
    {
        $text = 'error';
        $token = self::getToken();

        if ($token) {
            $url = 'https://crm.zoho.com/crm/private/json/Contacts/getRecords?authtoken='.$token.'&scope=crmapi&newFormat=1';
            $result = file_get_contents($url);

            if($result = json_decode($result)){
                if(isset($result->response->error->message)){
                    die($result->response->error->message);
                }

                foreach ($result->response->result->Contacts->row as $row) {
                    foreach ($row->FL as $fl) {
                        switch ($fl->val) {
                            case 'First Name':
                                $first_name = $fl->content;
                                break;
                            case 'Last Name':
                                $last_name = $fl->content;
                                break;
                            case 'Email':
                                $email = $fl->content;
                                break;
                            case 'Phone':
                                $phone = $fl->content;
                                break;
                        }
                    }

                    Contact::create([
                        'first_name' => $first_name,
                        'last_name' => $last_name,
                        'email' => $email,
                        'phone' => $phone,
                    ]);
                }

                $text =  'Contacts fetched successfully';
            } else {
                $text = 'Parsing error';
            }
        }

        return view('contact', ['text' => $text]);
    }

    public function getToken()
    {
        $token = config('zoho.token');

        if ($token) {
            return $token;
        } else {
            $config = config('zoho');
            $token = file_get_contents("https://accounts.zoho.com/apiauthtoken/nb/create?SCOPE=ZohoCRM/crmapi&EMAIL_ID=".$config['email']."&PASSWORD=".$config['password']."&DISPLAY_NAME=".$config['display_name']);
            if ($token) {
                preg_match('/AUTHTOKEN=([\S]+)/', $token, $token);
                if (isset($token[1]) && $token[1]) {
                    \Config::write('zoho.token', $token[1]);
                    return $token[1];
                }
            }

            return false;
        }
    }
}
