## Manual

To use application:

- Set database configuration in .env
- Run ```php artisan migrate``` 
- Run /contact/add to add contacts to zoho
- Run /contact/fetch to fetch contacts from zoho and add them to database
- Run vendor/phpunit/phpunit/phpunit in terminal for unittest