<?php

namespace Tests\Unit;

use Tests\TestCase;

class ContactTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->assertTrue(true);
    }

    public function testAddingExample()
    {
        $response = $this->get('/contact/add');
        $response->assertStatus(200);
        $response->assertSeeText('Record(s) added successfully');
        $response->assertDontSeeText('error');
        $response->assertDontSeeText('Unable to parse XML data');
    }

    public function testFetchExample()
    {
        $response = $this->get('/contact/fetch');
        $response->assertSeeText('Contacts fetched successfully');
        $response->assertDontSeeText('error');
        $response->assertDontSeeText('Parsing error');
    }
}
